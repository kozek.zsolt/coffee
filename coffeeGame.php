<?php

use CoffeeCompany\CoffeeMachine;
use CoffeeCompany\Store;
use CoffeeCompany\Coffee;

/**
 * @param CoffeeMachine $coffeeMachine
 * @param Store $store
 * @return void
 */
function addSugar(CoffeeMachine $coffeeMachine, Store $store) {
    try {
        if($store->isSugarInStock()) {
            $coffeeMachine->addSugar($store);
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

/**
 * @param CoffeeMachine $coffeeMachine
 * @param Store $store
 * @return void
 */
function addMilk(CoffeeMachine $coffeeMachine, Store $store) {
    try {
        if($store->isMilkInStock()) {
            $coffeeMachine->addMilk($store);
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

/**
 * @param CoffeeMachine $coffeeMachine
 * @param Store $store
 * @return void
 */
function addWhip(CoffeeMachine $coffeeMachine, Store $store) {
    try {
        if($store->isWhipInStock()) {
            $coffeeMachine->addWhip($store);
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

/**
 * @param CoffeeMachine $coffeeMachine
 * @param Store $store
 * @return void
 */
function accessoriesStep(CoffeeMachine $coffeeMachine, Store $store) {
    do {
        $inputChoose = readline('Please choose (s - sugar, m - milk, w - whip, x - exit): ');
        switch ($inputChoose) {
            case "s":
                addSugar($coffeeMachine, $store);

                break;
            case "m":
                addMilk($coffeeMachine, $store);

                break;
            case "w":
                addWhip($coffeeMachine, $store);

                break;
            case "x":
                $coffeeMachine->reset();

                break;
        }
    } while ($inputChoose != "x");
}

/**
 * @param CoffeeMachine $coffeeMachine
 * @param Coffee $coffee
 * @param Store $store
 * @return void
 */
function drinkStep(CoffeeMachine $coffeeMachine, Coffee $coffee, Store $store) {
    do {
        $inputPay = readline('Do you like to pay it? (y - yes, n - no)');
        switch ($inputPay) {
            case "y":
                $store->init();            

                $coffeeMachine->pay($coffee->getCost() + 100);
                $coffeeMachine->printEstimatedBalance();
                $coffeeMachine->printIngredients();

                accessoriesStep($coffeeMachine, $store);

                break;
        }
    } while ($inputPay != "n");
}