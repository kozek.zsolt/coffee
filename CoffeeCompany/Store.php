<?php

namespace CoffeeCompany;

use Exception;

class Store {
    private $defaultStock = 2;
    private $milkStock = 0;
    private $sugarStock = 0;    
    private $whipStock = 0;    

    /**
     * @return void
     */
    public function init() 
    {
        $this->milkStock = $this->defaultStock;
        $this->sugarStock = $this->defaultStock;        
        $this->whipStock = $this->defaultStock;                
    }    
    
    /**
     * @return bool
     */
    public function isMilkInStock(): bool 
    {
        if ($this->milkStock <= 0) {
            throw new Exception("Milk is sold out!\n");
        }
        
        return true;
    }
    
    /**
     * @return void
     */
    public function removeMilkFromStock() {
        $this->milkStock--;
    }
    
    /**
     * @return int
     */
    public function getMilkStock(): int 
    {
        return $this->milkStock;
    }
    
    /**
     * @return bool
     */
    public function isSugarInStock(): bool 
    {
        if ($this->sugarStock <= 0) {
            throw new Exception("Sugar is sold out!\n");
        }
        
        return true;
    }
    
    /**
     * @return void
     */
    public function removeSugarFromStock() {
        $this->sugarStock--;
    }
    
    /**
     * @return int
     */
    public function getSugarStock(): int 
    {
        return $this->sugarStock;
    }
    
    /**
     * @return bool
     */
    public function isWhipInStock(): bool 
    {
        if ($this->whipStock <= 0) {
            throw new Exception("Whip is sold out!\n");
        }
        
        return true;
    }
    
    /**
     * @return void
     */
    public function removeWhipFromStock() {
        $this->whipStock--;
    }
    
    /**
     * @return int
     */
    public function getWhipStock(): int 
    {
        return $this->whipStock;
    }
}