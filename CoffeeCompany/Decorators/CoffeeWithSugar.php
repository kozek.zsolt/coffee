<?php
/**
 * Created by PhpStorm.
 * User: gezamiklo
 * Date: 05/11/14
 * Time: 17:50
 */

namespace CoffeeCompany\Decorators;

use CoffeeCompany\Coffee;

class CoffeeWithSugar extends CoffeeDecorator {

    const EXTRA_COST = 5;

    /**
     * @param Coffee $decoratedCoffee
     */
    public function __construct(Coffee $decoratedCoffee) {
        parent::__construct($decoratedCoffee);
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return parent::getCost() + self::EXTRA_COST;
    }

    /**
     * @return string
     */
    public function getIngredients(): string
    {
        return parent::getIngredients() . ", Sugar";
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getIngredients();
    }
}
