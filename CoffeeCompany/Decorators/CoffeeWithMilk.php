<?php
/**
 * Created by PhpStorm.
 * User: gezamiklo
 * Date: 05/11/14
 * Time: 15:26
 */

namespace CoffeeCompany\Decorators;

use CoffeeCompany\Coffee;

class CoffeeWithMilk extends CoffeeDecorator{

    const EXTRA_COST = 30;

    /**
     * @param Coffee $decoratedCoffee
     */
    public function __construct(Coffee $decoratedCoffee) {
        parent::__construct($decoratedCoffee);
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return parent::getCost() + self::EXTRA_COST;
    }

    /**
     * @return string
     */
    public function getIngredients(): string
    {
        return parent::getIngredients() . ", Milk";
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getIngredients();
    }
} 