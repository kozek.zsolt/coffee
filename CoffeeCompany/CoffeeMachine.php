<?php
/**
 * Created by PhpStorm.
 * User: gezamiklo
 * Date: 05/11/14
 * Time: 15:29
 */

namespace CoffeeCompany;

use CoffeeCompany\Decorators\CoffeeDecorator;
use CoffeeCompany\Decorators\CoffeeWithMilk;
use CoffeeCompany\Decorators\CoffeeWithSugar;
use CoffeeCompany\Decorators\CoffeeWithWhip;
use CoffeeCompany\Coffee;
use CoffeeCompany\Store;

class CoffeeMachine {

    protected $coffe = null;
    protected $origCoffeType = null;    
    protected $money = 0;

    public function __construct()
    {
        echo "Welcome to CoffeeMachine!\n\n";
    }
    
    /**
     * @return void
     */
    public function reset() 
    {
        $this->money = 0;
        $this->coffe = $this->origCoffeType;   
    }

    /**
     * @param int $money
     * @return void
     */
    public function pay(int $money)
    {
        $this->money += (int)$money;
        echo "You dropped " . $this->money . " coin\n";
    }

    /**
     * @param Coffee $coffeeType
     * @return Coffee
     */
    public function getCoffee(Coffee $coffeeType): Coffee
    {
        $this->coffe = $coffeeType;
        $this->origCoffeType = $coffeeType;        
        
        return $this->coffe;
    }

    /**
     * @param Store $store
     * @return void
     */
    public function addMilk(Store $store)
    {
        $decorator = new CoffeeWithMilk($this->coffe); 
        $this->addAction($decorator, $store, 'Milk');
    }

    /**
     * @param Store $store
     * @return void
     */
    public function addSugar(Store $store)
    {
        $decorator = new CoffeeWithSugar($this->coffe);
        $this->addAction($decorator, $store, 'Sugar');
    }
    
    /**
     * @param Store $store
     * @return void
     */
    public function addWhip(Store $store)
    {
        $decorator = new CoffeeWithWhip($this->coffe);
        $this->addAction($decorator, $store, 'Whip');
    }
    
    /**
     * @param CoffeeDecorator $decorator
     * @param Store $store
     * @param string $type
     * @return void
     */
    private function addAction(CoffeeDecorator $decorator, Store $store, string $type) 
    {
        if ($this->money - $decorator->getCost() < 0) {
            $this->printLowMoneyMessage();
        } else {
            $removeStockFunction = 'remove' . $type . 'FromStock';
            $store->$removeStockFunction();
            
            $getStockFunction = 'get' . $type . 'Stock';
            $stock = $store->$getStockFunction();
            
            $this->coffe = $decorator;
            
            $this->printStock($stock, $type);
            $this->printEstimatedBalance();
            $this->printIngredients();
        }
    }
   
    /**
     * @return int
     */
    protected function getEstimatedBalance(): int
    {
        return $this->money - $this->coffe->getCost();
    }

    /**
     * @return void
     */
    public function printEstimatedBalance()
    {
        echo "You have " . $this->getEstimatedBalance() . " Fabatka(s) left\n";
    }

    /**
     * @return void
     */
    public function printIngredients()
    {
        //echo "Your coffee's ingredients are: " . $this->coffe->getIngredients() . "\n";
        echo "Your coffee's ingredients are: " . $this->coffe . "\n";
    }
    
    /**
     * @param string $type
     * @return void
     */
    public function printCoffeePrice(string $type) {
        echo "The " . $type . " price is " . $this->coffe->getCost() . " Fabatkas\n\n";
    }
    
    /**
     * @return void
     */
    private function printLowMoneyMessage() {
        echo "You have not enough money for this!\n";
    }
    
    /**
     * @return void
     */
    private function printStock($stock, $type) {
        echo "We have got " . $stock . " " . $type . " left!\n";
    }
} 