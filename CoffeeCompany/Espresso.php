<?php
/**
 * Created by PhpStorm.
 * User: gezamiklo
 * Date: 05/11/14
 * Time: 15:19
 */
namespace CoffeeCompany;

class Espresso extends Coffee {
// Extension of a simple coffee without any extra ingredients

    const COST = 300;

    /**
     * @return integer
     */
    public function getCost(): int
    {
        return self::COST;
    }

    /**
     * @return string
     */
    public function getIngredients(): string
    {
        return "Coffee, Water";
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getIngredients();
    }
}