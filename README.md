# README #

Feladatok:

1. Induláskor a Kávéautomata az Espresso árát írja ki, ne egy fix öszeget

2. Az Kávéautomata eleve fogadjon el összeget az inicializáláskor.

3. Csak addig lehessen rendelni, amíg van elég Fabatka.

4. Lehessen tejszínt is kérni a kávéba (Whip), mert az most nem működik.

5. Készíts egy raktárt, amiben a hozzávalók vannak. (Sugar, Milk, Whip). Egyszerű osztály elegendő, nem kell perzisztens tárolás, csak egy példány létezhet belőle.

6. Az egyes hozzávalókat a raktárból vegyük ki, ha elfogy, akkor a Store dobjon egy Exception-t a megfelelő üzenettel. Ezt az Kávéautomata kezelje le.

7. printIngredients működjön így: echo "Your coffee's ingredients are: " . $this->coffe . "\n";

8. Hozz létre 1 új kávétípust Capuccino néven! Hozzávalói: Water, Coffee, Milk, Whip. Készítsük fel a Kávéautomatát, hogy több kávétípust is tudjon fogadni, és tovább alakítani.



### What is this repository for? ###

* Central interview
* 1.0
