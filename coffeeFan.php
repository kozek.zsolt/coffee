<?php

require './CoffeeCompany/autoload.php';
require_once './coffeeGame.php';

use CoffeeCompany\CoffeeMachine;
use CoffeeCompany\Store;
use CoffeeCompany\Espresso;
use CoffeeCompany\Capuccino;

$coffeeMachine = new CoffeeMachine();
$store = new Store();
$store->init();

do {
    $inputCoffee = readline('What would you like to drink? (e - Espresso, c - Capuccino, x - Exit)');
    switch ($inputCoffee) {
        case "e":
            $coffee = $coffeeMachine->getCoffee(new Espresso());
            
            $coffeeMachine->printCoffeePrice('Espresso');
            drinkStep($coffeeMachine, $coffee, $store);
            
            break;
        
        case "c":
            $coffee = $coffeeMachine->getCoffee(new Capuccino());
           
            $coffeeMachine->printCoffeePrice('Capuccino');
            drinkStep($coffeeMachine, $coffee, $store);
            
            break;
    }
} while ($inputCoffee != "x");
        
            
            
            


